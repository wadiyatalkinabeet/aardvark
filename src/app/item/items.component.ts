import { Component, OnInit } from "@angular/core";

import { Item } from "./item";
import { ItemService } from "./item.service";
import { BarcodeScanner } from 'nativescript-barcodescanner';
import { Router } from '@angular/router';

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html"
})
export class ItemsComponent implements OnInit {
    items: Array<Item>;

    // This pattern makes use of Angular’s dependency injection implementation to
    // inject an instance of the ItemService service into this class.
    // Angular knows about this service because it is included in your app’s main NgModule,
    // defined in app.module.ts.
    constructor(private itemService: ItemService, private barcodeScanner: BarcodeScanner,
    private router: Router) { }

    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }

    requestPermission() {
        return new Promise((resolve, reject) => {
            this.barcodeScanner.available().then((available) => {
                if (available) {
                    this.barcodeScanner.hasCameraPermission().then((granted) => {
                        if (!granted) {
                            this.barcodeScanner.requestCameraPermission().then(() => {
                                resolve("Camera permission granted");
                            });
                        } else {
                            resolve("Camera permission was already granted");
                        }
                    });
                } else {
                    reject("This device does not have an available camera");
                }
            });
        });
    }

    scanBarcode() {
        this.requestPermission().then((result) => {
            this.barcodeScanner.scan({
                cancelLabel: "Stop scanning",
                message: "Go scan something",
                preferFrontCamera: false,
                showFlipCameraButton: true
            }).then((result) => {
                console.log("Scan format: " + result.format);
                console.log("Scan text:   " + result.text);
                this.itemService.addItem(
                    result.text
                );
            }, (error) => {
                console.log("No scan: " + error);
            });
        }, (error) => {
            console.log("ERROR", error);
        });
    }

    
    navigate_rewards(){
        this.router.navigate(['rewards']);
    }
}
