import { Injectable } from "@angular/core";

import { Item } from "./item";

@Injectable({
    providedIn: "root"
})
export class ItemService {
    id = 0;
    private items = new Array<Item>({
        id: 0,
        code: "example_code"
    });

    getItems(): Array<Item> {
        return this.items;
    }

    getItem(id: number): Item {
        return this.items.filter((item) => item.id === id)[0];
    }

    addItem(code: String) {
        this.items.push({
            id: 0,
            code: code
        });
    }
}
