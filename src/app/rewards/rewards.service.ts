import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class RewardsService {
    id = 0;
    private reward_items = [{
        id: 0,
        price: "1000",
        item_description: "new laptop etc etc etc",
        image_source: "https://images-na.ssl-images-amazon.com/images/I/416hQM57qXL._SX355_.jpg"
    },

    {
        id: 1,
        price: "1000",
        item_description: "new laptop etc etc etc",
        image_source: "https://images-na.ssl-images-amazon.com/images/I/416hQM57qXL._SX355_.jpg"
    },

    {
        id: 2,
        price: "1000",
        item_description: "new laptop etc etc etc",
        image_source: "https://images-na.ssl-images-amazon.com/images/I/416hQM57qXL._SX355_.jpg"
    }
    ];

    get_items() {
        return this.reward_items;
    }

}
