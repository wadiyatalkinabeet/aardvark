import { Component, OnInit } from '@angular/core';
import { RewardsService} from './rewards.service';

@Component({
  selector: 'ns-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.css'],
  moduleId: module.id,
})
export class RewardsComponent implements OnInit {

  items;

  constructor(
    private rewardsService: RewardsService
  ) { }

  ngOnInit() {
    this.items = this.rewardsService.get_items();
  }

  navigate_item(){
    
  }
}
